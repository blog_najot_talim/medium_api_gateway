package v1

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/blog_najot_talim/medium_api_gateway/api/models"
	pbp "gitlab.com/blog_najot_talim/medium_api_gateway/genproto/post_service"
	"net/http"
	"strconv"
)

// @Security ApiKeyAuth
// @Router /comments [post]
// @Summary Create a comment
// @Description Create a comment
// @Tags comment
// @Accept json
// @Produce json
// @Param comment body models.CreateCommentRequest true "comment"
// @Success 201 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateComment(c *gin.Context) {
	var (
		req models.CreateCommentRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	resp, err := h.grpcClient.CommentService().Create(context.Background(), &pbp.Comment{
		PostId:      req.PostID,
		UserId:      req.UserID,
		Description: req.Description,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	comment := parseCommentModel(resp)
	c.JSON(http.StatusCreated, comment)
}

// @Security ApiKeyAuth
// @Router /comments/{id} [put]
// @Summary Update comment
// @Description Update comment
// @Tags comment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param comment body models.UpdateCommentRequest true "Comment"
// @Success 201 {object} models.UpdateComment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateComment(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	var req models.UpdateCommentRequest

	if err = ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	comment, err := h.grpcClient.CommentService().Update(context.Background(), &pbp.Comment{
		Id:          id,
		Description: req.Description,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, parseCommentModel(comment))
}

// @Router /comments [get]
// @Summary Get all comments
// @Description Get all comments
// @Tags comment
// @Accept json
// @Produce json
// @Param filter query models.GetAllCommentsParams false "Filter"
// @Success 200 {object} models.GetAllCommentsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllComments(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.CommentService().GetAll(context.Background(), &pbp.GetAllCommentsRequest{
		Page:   int64(req.Page),
		Limit:  int64(req.Limit),
		Search: req.Search,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getCommentsResponse(result))
}

// @Security ApiKeyAuth
// @Router /comments/{id} [delete]
// @Summary Delete comment
// @Description Delete comment
// @Tags comment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteComment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.CommentService().Delete(context.Background(), &pbp.GetCommentRequest{Id: int64(id)})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "success",
	})
}

func parseCommentModel(comment *pbp.Comment) models.Comment {
	return models.Comment{
		ID:          comment.Id,
		UserID:      comment.UserId,
		PostID:      comment.PostId,
		Description: comment.Description,
		CreatedAt:   comment.CreatedAt,
		UpdatedAt:   comment.UpdatedAt,
		User: models.CommentUser{
			FirstName:       comment.User.FirstName,
			LastName:        comment.User.LastName,
			Email:           comment.User.Email,
			ProfileIMageUrl: comment.User.ProfileImageUrl,
		},
	}
}

func getCommentsResponse(data *pbp.GetAllCommentsResponse) *models.GetAllCommentsResponse {
	response := models.GetAllCommentsResponse{
		Comments: make([]*models.Comment, 0),
		Count:    data.Count,
	}

	for _, comment := range data.Comments {
		u := parseCommentModel(comment)
		response.Comments = append(response.Comments, &u)
	}

	return &response
}
