package v1_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blog_najot_talim/medium_api_gateway/api/models"
)

func createComment(t *testing.T) *models.Comment {
	authUser := loginUser(t)
	post := createPost(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Comment{
		UserID: authUser.ID,
		PostID: post.ID,
		Description: faker.Sentence(),
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/comments", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return nil
}

func TestCreateComment(t *testing.T) {
	createComment(t)
}

func TestGetAllComments(t *testing.T) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET","/v1/comments", nil)
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	fmt.Println(resp.Body.String())
}

func TestGetAllCommentsCases(t *testing.T) {
	testCases := []struct {
		name string
		query string
		checkResponse func(t *testing.T, recoder *httptest.ResponseRecorder)

	}{
		{
			name: "success case",
			query: "?limit=10&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
		{
			name: "incorrect limit param",
			query: "?limit=ads&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			url := fmt.Sprintf("/v1/comments%s", tc.query)

			req, _ := http.NewRequest("GET", url, nil)
			router.ServeHTTP(resp, req)

			tc.checkResponse(t, resp)
		})
	}
}