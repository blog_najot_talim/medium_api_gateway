package v1_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/blog_najot_talim/medium_api_gateway/api/models"
)

func createLike(t *testing.T) *models.CreateOrUpdateLikeRequest {
	authUser := loginUser(t)
	post := createPost(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.CreateOrUpdateLikeRequest{
		PostID: post.ID,
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/likes", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return nil
}

func TestCreateLike(t *testing.T) {
	createPost(t)
}