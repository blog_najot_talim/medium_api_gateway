package v1_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blog_najot_talim/medium_api_gateway/api/models"
)

func createPost(t *testing.T) *models.Post {
	authUser := loginUser(t)
	category := createCategory(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Post{
		Title:       faker.Sentence(),
		Description: faker.Sentence(),
		ImageUrl:    faker.URL(),
		UserID:      authUser.ID,
		CategoryID:  category.ID,
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/posts", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return nil
}

func TestCreatePost(t *testing.T) {
	createPost(t)
}

func TestGetPost(t *testing.T) {
	g := createPost(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/posts/%d", g.ID)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestGetAllPosts(t *testing.T) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/posts", nil)
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	fmt.Println(resp.Body.String())
}

func TestGetAllPostsCases(t *testing.T) {
	testCases := []struct {
		name          string
		query         string
		checkResponse func(t *testing.T, recoder *httptest.ResponseRecorder)
	}{
		{
			name:  "success case",
			query: "?limit=10&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
		{
			name:  "incorrect limit param",
			query: "?limit=ads&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			url := fmt.Sprintf("/v1/posts%s", tc.query)

			req, _ := http.NewRequest("GET", url, nil)
			router.ServeHTTP(resp, req)

			tc.checkResponse(t, resp)
		})
	}
}

func TestUpdatePost(t *testing.T) {
	g := createPost(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.UpdatePostRequest{
		Title:       g.Title,
		Description: g.Description,
		ImageUrl:    g.ImageUrl,
		UserID:      g.UserID,
		CategoryID:  g.CategoryID,
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/posts/:id", bytes.NewBuffer(payload))
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
}
